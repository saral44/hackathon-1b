package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Stock;

@Repository
public class MYSQLStockRepository implements StockRepository {
	
	@Autowired
	JdbcTemplate template;

	@Override
	public List<Stock> getAllStocks() {
		String sql = "SELECT * FROM stockinfo";
		return template.query(sql, new StockRowMapper());
	}

	@Override
	public Stock getStockById(int id) {
		String sql = "SELECT * FROM stockinfo WHERE id=?";
		return template.queryForObject(sql, new StockRowMapper(), id);
	}

	@Override
	public Stock editStock(Stock stock) {
		String sql = "UPDATE stockinfo SET stockTicker = ?, price = ?, volume = ?, buyOrSell = ?, statusCode = ? " +
				"WHERE id = ?";
		template.update(sql,stock.getStockTicker(),stock.getPrice(),stock.getVolume(),stock.getBuyOrSell(),stock.getStatusCode(),stock.getId());
		return stock;
	}

	@Override
	public int deleteStock(int id) {
		String sql = "DELETE FROM stockinfo WHERE id = ?";
		template.update(sql,id);
		return id;
	}

	@Override
	public Stock addStock(Stock stock) {
		String sql = "INSERT INTO stockinfo(stockTicker, price, volume, buyOrSell, statusCode) " +
				"VALUES(?,?,?,?,?)";
		template.update(sql,stock.getStockTicker(),stock.getPrice(),stock.getVolume(),stock.getBuyOrSell(),stock.getStatusCode());
		return stock;
	}
	
	class StockRowMapper implements RowMapper<Stock>{

		@Override
		public Stock mapRow(ResultSet rs, int rowNum) throws SQLException{
			return new Stock(rs.getInt("id"),
					rs.getString("stockTicker"),
					rs.getDouble("price"),
					rs.getInt("volume"),
					rs.getString("buyOrSell"),
					rs.getInt("statusCode"));
		}
	}
}
