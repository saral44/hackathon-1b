CREATE TABLE stockinfo (
id int auto_increment Primary Key ,
stockTicker varchar(40) Not Null,
price double NOT NULL,
volume int NOT NULL,
buyOrSell varchar(40) NOT NULL,
statusCode int NOT NULL
);

INSERT INTO stockinfo VALUES (201,"AMZN",4500,100,"BUY",0);
insert into stockinfo values (202,"NFLX",5000,200, "SELL",1);
insert into stockinfo values (203,"HULU",6000,300, "BUY",0);
insert into stockinfo values (204,"TARGET",7000,400, "BUY",2);