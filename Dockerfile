FROM openjdk:11.0.12-jre-slim

COPY target/Demo_Hackathon-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app.jar"]
